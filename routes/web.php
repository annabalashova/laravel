<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/user/{id?}', 'UserController@index');
Route::get('/post/create', 'PostController@create');
Route::get('post/{post?}', 'PostController@show');
Route::match(['get', 'post'], '/apinasa/show', 'ApiNasaController@show');
Route::match(['get', 'post'], '/apinasa/get', 'ApiNasaController@get');

