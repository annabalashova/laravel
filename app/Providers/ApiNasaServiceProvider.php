<?php


namespace App\Providers;


use App\Services\ApiNasa;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\ServiceProvider;

class ApiNasaServiceProvider extends ServiceProvider
{
public function register()
{
  $this->app->bind('client', function ($app){
      return new Client();
  });
   $this->app->bind('apiNasa', function ($app){
       return new ApiNasa(app()->get('client'));
   });
}
}
