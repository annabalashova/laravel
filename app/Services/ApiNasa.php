<?php


namespace App\Services;


use App\Models\Image;
use GuzzleHttp\ClientInterface;

class ApiNasa
{
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function getResponse($date)
    {
        $response = $this->client->request(
            'GET',
            "https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&date=$date");
        return $response;
    }
}
