<?php


namespace App\Http\Controllers;


use App\Models\Image;
use App\Services\ApiNasa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiNasaController extends Controller
{
    public $date;
    public $text;
    public $url;
    public $explanation;
    public $type;

    public $image;

    public function show()
    {
        return view('pages.formdate');
    }

    public function get(Request $request)
    {
        if ($request->isMethod('post')) {
            $date = $request->date;
        }

        $this->read($date);

        if ($this->image) {
            $this->url = $this->image->url;
            $this->type = $this->image->type;
            //print_r($this->image);
        } else {
            $api = app()->get('apiNasa');
            $response = json_decode($api->getResponse($date)->getBody(), true, 512);

            $this->date = $date;
            $this->text = $response['url'];
            $this->explanation = $response['explanation'];
            $this->type = $response['media_type'];
            $this->url = $response['url'];
            $this->create();
        }

        if ($this->type == 'image') {
            return view('pages.image', ['url' => $this->url]);
        } elseif ($this->type == 'video') {
            return view('pages.video', ['url' => $this->url]);
        }
    }

    public function create()
    {
        $image = new Image();
        $image->date = $this->date;
        $image->url = $this->url;
        $image->type = $this->type;
        $image->explanation = $this->explanation;
        if ($image->save()) {
            echo 'Image create';
        }
    }

    public function read($date)
    {
        $this->image = DB::table('images')->where('date', $date)->first();
    }
}

