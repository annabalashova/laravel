
<html>
<head>
    <title>@yield('title')</title>
    @include('includes.style')
</head>
<body>
@section('sidebar')

@show
<div class="forms">
@yield('component')
</div>

<div class="container">
    @yield('content')
</div>
</body>
</html>
