@extends('layouts.main')

@section('component')
    <a href="/apinasa/show">Назад</a>
    <iframe width="560" height="315" src="{{ $url }}" frameborder="0" allowfullscreen>
    </iframe>
@endsection
