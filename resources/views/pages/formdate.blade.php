@extends('layouts.main')

@section('component')
    <form method="post" action="/apinasa/get" autocomplete="on">
        {{ csrf_field() }}
        <label for="date">Введите дату</label>
        <input type="date" id="date" name="date"/>
        <br>
        <button type="submit">Отправить</button>
    </form>
@endsection

